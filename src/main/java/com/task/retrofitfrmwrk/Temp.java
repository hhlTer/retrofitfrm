package com.task.retrofitfrmwrk;

import com.google.gson.Gson;
import com.task.retrofitfrmwrk.fabric.GitHubRetrofit;
import com.task.retrofitfrmwrk.fabric.RetrofitFabric;
import com.task.retrofitfrmwrk.service.RepositoryService;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.util.List;

public class Temp {
//    public static void main(String[] args) throws IOException {
//
//        GitHubRetrofit retrofit = new RetrofitFabric().getRetrofitController(GitHubRetrofit.class);
//        RepositoryService repositoryService = retrofit.getRepositoryService();
//        Call<List<Object>> oktocot = repositoryService.listRepos("oktocat");
//        Response<List<Object>> execute = oktocot.execute();
//        System.out.println();
//    }

    public static void main(String[] args) throws IOException {
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .baseUrl("https://api.github.com/")
                .build();

        RepositoryService service = retrofit.create(RepositoryService.class);
        Call<List<Object>> oktocot = service.listRepos("oktocat");
        Response<List<Object>> result = oktocot.execute();
        System.out.println(result);
    }
}
