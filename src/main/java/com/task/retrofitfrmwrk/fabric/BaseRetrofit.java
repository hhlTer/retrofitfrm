package com.task.retrofitfrmwrk.fabric;

import lombok.Getter;
import retrofit2.Retrofit;

@Getter
public class BaseRetrofit {
    private Retrofit retrofit;
}
