package com.task.retrofitfrmwrk.fabric;


import com.task.retrofitfrmwrk.annotations.Inject;
import com.task.retrofitfrmwrk.service.RepositoryService;

public class GitHubRetrofit extends BaseRetrofit{

    @Inject
    private RepositoryService repositoryService;

    public RepositoryService getRepositoryService(){
        return repositoryService;
    }

}
