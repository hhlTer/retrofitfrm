package com.task.retrofitfrmwrk.fabric;

import com.task.retrofitfrmwrk.annotations.Configuration;
import com.task.retrofitfrmwrk.annotations.Inject;
import com.task.retrofitfrmwrk.annotations.Parameter;
import com.task.retrofitfrmwrk.annotations.RetrofitBuilder;
import com.task.retrofitfrmwrk.exception.DuplicateConfigurationException;
import com.task.retrofitfrmwrk.exception.WrongConfigurationException;
import com.task.retrofitfrmwrk.utils.PackageScanner;
import com.task.retrofitfrmwrk.utils.PropertiesProvider;
import org.reflections.Reflections;
import org.reflections.scanners.Scanners;
import retrofit2.Retrofit;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class RetrofitFabric {

    private final Map<Class<? extends BaseRetrofit>, BaseRetrofit> repo;
    private final Map<Class<?>, Retrofit> configurationMap;

    public RetrofitFabric() {
        this.repo = new HashMap<>();
        this.configurationMap = new ConcurrentHashMap<>();
        scan();
    }

    public <T extends BaseRetrofit> T getRetrofitController(Class<T> clazz) {
        return (T) repo.computeIfAbsent(clazz, o -> {
            try {
                T result = (T) o.getDeclaredConstructor().newInstance();
                Field[] fields = result.getClass().getDeclaredFields();
                instantiateServices(result, fields);
//                instantiateRetrofit(result, fields);
                return result;
            } catch (Exception e) {
                throw new RuntimeException(e.getMessage());
            }
        });
    }

    private <T extends BaseRetrofit> void instantiateRetrofit(T result, Field[] fields) {
        Arrays.stream(fields).filter(field -> field.getName().equals("retrofit"))
                .forEach(field -> {
                    Retrofit retrofit = configurationMap.get(field.getDeclaringClass());
                    field.setAccessible(true);
                    try {
                        field.set(result, retrofit);
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                });
    }

    private <T extends BaseRetrofit> void instantiateServices(T retrofitController, Field[] fields) {
        Arrays.stream(fields)
                .filter(f -> f.isAnnotationPresent(Inject.class))
                .forEach(field -> {
                    Class declaringClass = field.getDeclaringClass();
                    Class declaringService = field.getType();
                    try {
                        Retrofit retrofit = configurationMap.get(declaringClass);
                        Object o1 = retrofit.create(declaringService);
                        field.setAccessible(true);
                        field.set(retrofitController, o1);
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                });
    }

    private void scan() {
        List<Class<?>> configurationSet = null;
        try {
            configurationSet = PackageScanner.findClasses(
                    "com.task.retrofitfrmwrk", null, Configuration.class
            );
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        configurationSet.stream()
                .map(clazz -> {
                    try {
                        return clazz.getDeclaredConstructor().newInstance();
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                })
                .forEach(configObject -> {
                    Class<?> aClass = configObject.getClass();
                    initFieldsByParameter(configObject, aClass);
                    Map<Class<?>, Retrofit> classRetrofitMap = buildRetrofitConfiguration(configObject, aClass);
                    if (configurationMap.keySet().stream().anyMatch(classRetrofitMap::containsKey)) {
                        throw new DuplicateConfigurationException(aClass);
                    }
                    configurationMap.putAll(classRetrofitMap);
                });
    }

    private Map<Class<?>, Retrofit> buildRetrofitConfiguration(Object configObject, Class<?> aClass) {
        Method[] methods = aClass.getMethods();
        Map<Class<?>, Retrofit> retrofitMap = new HashMap<>();
        Arrays.stream(methods)
                .filter(method -> method.isAnnotationPresent(RetrofitBuilder.class))
                .forEach(method -> {
                    if (!method.getReturnType().equals(Retrofit.class)) {
                        throw new WrongConfigurationException(
                                String.format(
                                        "Unexpected return type of the method %s::%s", aClass.getName(), method.getName()));
                    }
                    Retrofit retrofit = null;
                    try {
                        retrofit = (Retrofit) method.invoke(configObject);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        throw new RuntimeException(e);
                    }
                    RetrofitBuilder annotation = method.getAnnotation(RetrofitBuilder.class);
                    retrofitMap.put(annotation.clazz(), retrofit);
                });
        return retrofitMap;
    }

    private void initFieldsByParameter(Object configObject, Class<?> aClass) {
        Field[] fields = aClass.getDeclaredFields();
        Arrays.stream(fields)
                .filter(field -> field.isAnnotationPresent(Parameter.class))
                .forEach(field -> {
                    String value = PropertiesProvider.propertiesHub.get(field.getAnnotation(Parameter.class).name());
                    try {
                        field.setAccessible(true);
                        field.set(configObject, value);
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                });
    }

    private Set<Class<?>> getAllConfigurationClasses(Package[] packages) {
        return Arrays.stream(packages)
                .flatMap(pack -> findAllClasses(pack.getName()).stream())
                .filter(clazz -> clazz.isAnnotationPresent(Configuration.class))
                .collect(Collectors.toSet());
    }

    private Set<Class<?>> findAllClasses(String packageName) {
        Reflections reflections = new Reflections(packageName, Scanners.SubTypes);
        return reflections.getSubTypesOf(Object.class);
    }
}
