package com.task.retrofitfrmwrk.configuration;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.task.retrofitfrmwrk.annotations.Configuration;
import com.task.retrofitfrmwrk.annotations.Parameter;
import com.task.retrofitfrmwrk.annotations.RetrofitBuilder;
import com.task.retrofitfrmwrk.fabric.GitHubRetrofit;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Configuration
public class GitHubConfig {

    @Parameter(name = "github.url")
    private String url;

    @RetrofitBuilder(clazz = GitHubRetrofit.class)
    public Retrofit gitHubRetrofit(){
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        return new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }
}
