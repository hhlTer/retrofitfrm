package com.task.retrofitfrmwrk.exception;

public class WrongConfigurationException extends RuntimeException {
    public WrongConfigurationException(String message) {
        super(message);
    }
}
