package com.task.retrofitfrmwrk.exception;

public class DuplicateConfigurationException extends RuntimeException {
    public DuplicateConfigurationException(Class<?> aClass) {
        super("Duplicate configuration is not allowed. Class: " + aClass.getName());
    }
}
