package com.task.retrofitfrmwrk.exception;

public class PropertyFileRequiredException extends RuntimeException {
    public PropertyFileRequiredException(String propertyFileName) {
        super("The property file is required in the resources folder: " + propertyFileName);
    }
}
