package com.task.retrofitfrmwrk.utils;

import com.task.retrofitfrmwrk.exception.PropertyFileRequiredException;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PropertiesProvider {

    private static final String PROPERTY_FILE_NAME = "retrofit.properties";
    public static Map<String,String> propertiesHub;

    static {
        String path = PropertiesProvider.class.getClassLoader().getResource(PROPERTY_FILE_NAME).getPath();
        if(path == null){
            throw new PropertyFileRequiredException(PROPERTY_FILE_NAME);
        }
        Stream<String> lines = null;
        try {
            lines = new BufferedReader(new FileReader(path)).lines();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        propertiesHub = lines
                .map(line -> line.split("="))
                .collect(Collectors.toMap(arr -> arr[0].trim(), arr -> arr[1].trim()));
    }
}
