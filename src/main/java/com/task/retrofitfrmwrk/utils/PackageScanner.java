package com.task.retrofitfrmwrk.utils;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class PackageScanner {
    public static List<Class<?>> findClasses(String packageName, ClassLoader loader, Class<? extends Annotation> annotation) throws IOException {
        List<Class<?>> ret = new ArrayList<>();
        if (loader == null)
            loader = Thread.currentThread().getContextClassLoader();
        String path = packageName.replace('.', '/');
        try {
            Enumeration<URL> res = loader.getResources(path);
            while (res.hasMoreElements()) {
                String dirPath = URLDecoder.decode(res.nextElement()
                        .getPath(), "UTF-8");
                File dir = new File(dirPath);
                for (File file : dir.listFiles()) {
                    if (file.isDirectory())
                        ret.addAll(findClasses(packageName + '.' + file.getName(), loader, annotation));
                }
            }
        } catch (IOException e) {
            // We failed to get any nested directories. State
            // so and continue; this directory may still have
            // some classes.
        }
        URL tmp = loader.getResource(path);
        if (tmp == null)
            return ret;
        File currDir = new File(tmp.getPath());
        for (String classFile : currDir.list()) {
            if (classFile.endsWith(".class")) {
                try {
                    Class<?> add = Class.forName(packageName + '.' +
                            classFile.substring(0, classFile.length() - 6));
                    if (add.isAnnotationPresent(annotation))
                        ret.add(add);
                } catch (NoClassDefFoundError e) {
                    // The class loader could not load the class
                } catch (ClassNotFoundException e) {
                    // We couldn't even find the damn class
                }
            }
        }
        return ret;
    }
}
