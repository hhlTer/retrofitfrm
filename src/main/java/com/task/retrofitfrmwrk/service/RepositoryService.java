package com.task.retrofitfrmwrk.service;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

import java.util.List;

public interface RepositoryService {
    @GET("users/{user}/repos")
    Call<List<Object>> listRepos(@Path("user") String user);
}
